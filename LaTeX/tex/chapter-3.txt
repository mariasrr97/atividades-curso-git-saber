% 2
     Thou, O God! who sellest us all good things at the price of labor.
                                                    --LEONARDO DA VINCI.


Closely following his arrival in Vienna, Beethoven began studying
composition with Haydn, applying himself with great diligence to the
work in hand; but master and pupil did not get along together very well.
There were many dissonances from the start. It was not in the nature of
things that two beings so entirely dissimilar in their point of view
should work together harmoniously. Beethoven, original, independent,
iconoclastic, acknowledged no superior, without having as yet achieved
anything to demonstrate his superiority; Haydn, tied down to established
forms, subservient, meek, was only happy when sure of the approbation of
his superiors. His attitude toward those above him in rank was
characterized by respect and deference; he probably expected something
similar from Beethoven toward himself. Haydn was then at the height of
his fame, courted and admired by all, and his patience was sorely tried
by the insolence of his fiery young pupil. He nicknamed Beethoven the
Grand Mogul, and did not have much good to say of him to others. The
pittance which he received for these lessons was no inducement to him,
as he was in receipt of an income much beyond his requirements. The time
given up to these lessons could have been better employed in composing.

Haydn and Beethoven, however, were in a measure supplementary to one
another as regards the life-work of each. Haydn paved the way for
Beethoven, who was his successor in the large orchestral forms. He and
also Mozart were pioneers in the field which Beethoven made peculiarly
his own. Haydn also directed Beethoven's attention to the study of
Händel and Bach, whose works Beethoven always held most highly in
esteem. It is true that Beethoven, even in the old Bonn days, was
familiar to some extent with the works of these masters; but his
opportunity for getting at this kind of music was limited in Bonn.
Vienna, the musical center of the world at that time, was, as may be
supposed, a much better field in this respect. The study of these
profound works of genius under the leadership and eulogy of so prominent
a musician as Haydn had much to do with shaping Beethoven's ideals.
These masters gave an example of solidity and earnestness which is
characteristic of their work. Haydn and Mozart, on the other hand,
appealed to him in his lighter moods, in the play of fancy, in the
capricious and humorous conceits of which he has given such fine
examples in the symphonies and sonatas.

The lessons to Beethoven continued for a little over a year, or until
Haydn left on another visit to England in January of 1794. So eager was
he for advancement, that he took lessons from another teacher at the
same time, carefully concealing the fact from Haydn. Beethoven always
maintained that he had not learned much from him.

Strangely, Haydn had no idea at this time or for some years after that
his pupil would ever amount to much in musical composition. He lived
long enough to find Beethoven's position as a musician firmly
established, but not long enough to witness his greatest triumphs.

On the departure of Haydn he began with Albrechtsberger in composition,
also having violin, and even vocal lessons from other masters. Beethoven
realized, on coming to Vienna, more fully than before, the necessity for
close application to his studies. Though a finished performer, he knew
but little of counterpoint, and the more purely scientific side of his
art had been neglected. That he applied himself with all the ardor of
his nature to his studies we know. They were given precedence over
everything else. He even delayed for a long while writing a rondo which
he had promised to Eleonore von Breuning and when he finally sent it, it
was with an apology for not sending a sonata, which had also been
promised.

It is characteristic of Beethoven that his teachers in general were not
greatly impressed by him. We have seen how it was in the case of Haydn.
Albrechtsberger was more pronounced in his disapproval. "He has learned
nothing; he never will learn anything," was his verdict regarding
Beethoven. This was surely small encouragement. Beethoven's original and
independent way of treating musical forms brought on this censure. As he
advanced in musical knowledge he took the liberty to think for himself;
a very culpable proceeding with teachers of the stamp of
Albrechtsberger. The young man's intuitive faculties, the surest source
of all knowledge according to Schopenhauer, were developed to an
abnormal degree. By the aid of this inner light he was able to see truer
and farther than his pedantic old master, with the result that the pupil
would argue out questions with him on subjects connected with his
lessons which subverted all discipline, and well-nigh reversed their
relative positions. Beethoven's audacity--his self-confidence, is
brought out still more strongly when we reflect on the distinguished
position held by Albrechtsberger, both as teacher and composer. He was
director of music at St. Stephen's and was in great demand as a teacher.
Some of his pupils became distinguished musicians, among them Hümmel,
Seyfried and Weigl. He excelled in counterpoint, and was a prolific
composer, although his works are but little known at the present day. He
was set in his ways, a strict disciplinarian, conservative to the
backbone, and upward of sixty years of age. We can readily believe there
were stormy times during these lessons. There is no doubt however, that
Beethoven learned a great deal from him, as is evident from the
exercises still in existence from this period, embracing the various
forms of fugue and counterpoint, simple, double, and triple, canon and
imitation. He was thorough in his teaching and Beethoven was eager to
learn, so they had at least one point in common, and the pupil made
rapid headway. But his originality and fertility in ideas, which showed
itself at times in a disregard for established forms when his genius was
hampered thereby--qualities which even in Albrechtsberger's lifetime
were to place his pupil on a pinnacle above all other composers of the
period, were neither understood nor approved by the teacher. Under the
circumstances, it is not surprising that the lessons continued but
little over a year. His studies in theory and composition seem to have
come to an end with Albrechtsberger; we hear of no other teacher having
been engaged thereafter.

Shortly after Beethoven came to Vienna, his father died, and soon after
the two brothers Johann and Caspar, having no ties to keep them in Bonn,
followed the elder brother, who kept a fatherly watch over them. They
gave him no end of trouble for the rest of his life, but Beethoven bore
the burden willingly and was sincerely attached to them. All the honor
and nobility of the family seems to have centered in him.

On his arrival in Vienna he carried letters of introduction from Count
Waldstein and from the Elector, which opened to him the doors of the
best houses. His intrinsic worth did the rest. One of his earliest
Vienna friends was Prince Lichnowsky, a person who seems to have
possessed a combination of all those noble qualities that go to make up
the character of a gentleman. Highly cultivated and enthusiastic on the
subject of music, he had the penetration to see that in Beethoven he had
before him one of the elect of all time. The Prince had been a pupil of
Mozart and an ardent admirer of the deceased master. Providentially,
Beethoven appeared on the scene soon after Mozart's decease, and
received the devotion and admiration that had formerly been given
Mozart. In this he was ably seconded by his wife, who shared with him
the admiration and reverential wonder which such highly endowed people
would be apt to accord to a man of genius. One of the first acts of
this princely couple was to give Beethoven a pension of 600 florins per
year. This was but the beginning of unexampled kindness on their part.
They followed this by giving him a home in their residence on the
Schotten bastion, and we find him well launched in the social life of
the gayest capital in Europe.

This practical help was invaluable to Beethoven, for with the aid which
he had from the Elector, it was almost enough to assure him
independence. It not only increased his opportunities for study, but,
his mind being free from care, he was enabled to profit more by his
studies. The Lichnowskys were older than Beethoven and were childless.
He was allowed to do as he pleased; a privilege of which he availed
himself without hesitation. They entertained considerably and their
social position was unexceptionable. They maintained a small orchestra
for the performance of the music he liked and for his own compositions.
He was always the honored guest, and met the best people of Vienna. The
devotion of the Princess, in particular, was always in evidence.

It can be readily understood that with such an original character as
Beethoven, headstrong and impatient of restraint, a pleasant smooth life
was not to be expected. The arrangement would seem to have been an
excellent one for him, but he did not so regard it. Already at odds with
the world, misunderstanding people and being misunderstood, he soon came
to realize that a life of solitude was the only resource for a man
constituted as he was. He never considered himself under any obligation
to the Prince, or rather, he acted as though he felt the obligation to
be the other way. He acted independently from the start, taking his
meals at a restaurant whenever it suited his convenience, and showing an
ungovernable temper when interfered with in any way. But the kindness
and patience of the Princess never failed her; after any trouble it was
she who smoothed the difficulty and restored harmony. She was like an
indulgent mother to him; in her eyes he could do no wrong.

Prince Lichnowsky was wholly unaccustomed to this sort of thing. It is
certain that he never met with anything of the kind from Mozart, and
there were times when his patience was sorely tried by Beethoven. The
Princess, with a sweetness and graciousness which Beethoven appreciated,
always made peace between them. He afterward said that her solicitude
was carried to such a length that she wished to put him under a glass
shade, "that no unworthy person might touch or breathe on me."

Of course this kind of thing only confirmed the young man in his course.
It was kindness, but it was not wisdom. Few people are so constituted as
to be able to stand praise and adulation without the character suffering
thereby. Censure would have been much better for him. When the
individual is attacked, when he is made to assume the defensive, he
first discovers the vulnerable points in his armor, and as opportunity
offers strengthens them. Beethoven's ungovernable temper and apparent
ingratitude are frequently commented on, but the ingratitude was only
apparent. When he came to a knowledge of himself and discovered that he
was in the wrong in any controversy or quarrel, and it must be admitted
they were frequent enough all through his life, he would make amends for
it so earnestly, with such vehement self-denunciation, and show such
contrition, that it would be impossible for any of his friends to hold
out against him. Then there would be a short love-feast, during which
the offended party would possibly be the recipient of a dedication from
the master, and things would go on smoothly until the next break. The
Prince soon learned to make all sorts of concessions to his headstrong
guest, and even went so far as to order his servant to give Beethoven
the precedence, in case he and Beethoven were to ring at the same time.

But Beethoven did not like the new life. Even the little restraint that
it imposed was irksome to him, and the arrangement came to an end in
about two years. But the friendship continued for many years.
Beethoven's opus 1 is dedicated to the Prince, as well as the grand
Sonata Pathetique, and the Second Symphony, also the opus 179,
consisting of nine variations, and the grand Sonata in A Flat. To the
Princess Lichnowsky he dedicated opus 157, variations on "See the
Conquering Hero Comes." He also dedicated several of his compositions to
Count Moritz Lichnowsky, a younger brother of the Prince.

Among the other friends of this period may be mentioned Prince
Lobkowitz, who was an ardent admirer of Beethoven, Prince Kinski, and
also Count Browne to whose wife Beethoven dedicated the set of Russian
variations. In acknowledgment of this honor, the Count presented
Beethoven with a horse. He accepted it thankfully and then forgot all
about it until some months after, when a large bill came in for its
keep. There was also Count Brunswick and the Baron von Swieten, and most
of the music-loving aristocracy of Vienna, who it appears could not see
enough of him. His music and his individuality charmed them and he was
beset with invitations. Baron von Swieten was one of his earliest and
staunchest friends. His love and devotion to music knew no bounds. He
gave concerts at his residence with a full band, and produced music of
the highest order, Händel and Sebastian Bach being his favorites, the
music being interpreted in the best manner. It is related that the old
Baron would keep Beethoven after the others had left, making him play
far into the night and would sometimes put him up at his own house so
that he might keep him a little longer. A note from the Baron to
Beethoven is preserved, in which he says, "If you can call next
Wednesday I shall be glad to see you. Come at half-past eight in the
evening with your nightcap in your pocket."

These social successes, however, did not lead to idleness. He kept up
the practise all his life of recording his musical thoughts in
sketch-books, which latter are an object lesson to those engaged in
creative work as showing the extraordinary industry of the man and his
absorption in his work. Many of these are preserved in the different
museums, those in the British Museum being a notable collection. Some of
the work of this period was afterwards utilized by being incorporated
into the work of his riper years.

Beethoven's talents as a performer were freely acknowledged by all with
whom he came in contact. When we come to the question of his creative
talent, we can only marvel at the slowness with which his powers
unfolded themselves. His opus 1 appeared in 1795, when he was
twenty-four years old. There was nothing of the prodigy about him in
composition. At twenty-four, Mozart had achieved some of his greatest
triumphs.

Beethoven's work however, shows intellectuality of the highest kind, and
this, whether in music or literature, is not produced easily or
spontaneously; it is of slow growth, the product of a ripened mind,
attained only by infinite labor and constant striving after perfection,
with the highest ideals before one.

He had been trying his hand at composition for many years, but was
always up to this time known as a performer rather than as a composer,
although he frequently played his own compositions, and had as we have
seen, great talent at improvising, which in itself is a species of
composition, and an indication of musical abilities of the highest
order.

All the great masters of music delighted in the exercise of this talent,
although it is now rarely attempted in public, Chopin having been one of
the last to exercise it. Bach excelled in it, sometimes developing
themes in the form of a fugue at a public performance. No preparation
would be possible under these circumstances, as in many cases the theme
would be given by one of the audience.

This art of improvising, as these masters practised it,--who can explain
it or tell how it is done? All we know is that the brain conceives the
thought, and on the instant the fingers execute it in ready obedience to
the impulse sent out by the brain, the result being a finished
performance, not only so far as the melody is concerned, but in harmony
and counterpoint as well. Mozart, at the age of fourteen, at Mantua, on
his second Italian tour, improvised a sonata and fugue at a public
concert, taking the impressionable Italians by storm, and such
performances he repeated frequently in after years. Beethoven excelled
in this direction as greatly as he afterward did in composition,
towering high over his contemporaries. Czerny, pupil of Beethoven and
afterward teacher of Liszt, states that Beethoven's improvisations
created the greatest sensation during the first few years of his stay in
Vienna. The theme was sometimes original, sometimes given by the
auditors. In Allegro movements there would be bravura passages, often
more difficult than anything in his published works. Sometimes it would
be in the form of variations after the manner of his Choral Fantasia,
op. 80, or the last movement of the Choral Symphony. All authorities
agree as to Beethoven's genius in improvising. His playing was better
under these circumstances than when playing a written composition, even
when it was written by himself.

Once Hümmel undertook a contest with Beethoven in improvising. After he
had been playing for some time Beethoven interrupted him with the
question, "When are you going to begin?" It is needless to say that
Beethoven, when his turn came to play, distanced the other so entirely
that there was no room for comparison.